/*
 * Copyright (C) 2012-2013 Freescale Semiconductor, Inc. All Rights Reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <linux/types.h>
#include <linux/sched.h>
#include <linux/delay.h>
#include <linux/pm.h>
#include <linux/interrupt.h>
#include <linux/irq.h>
#include <linux/init.h>
#include <linux/input.h>
#include <linux/nodemask.h>
#include <linux/clk.h>
#include <linux/platform_device.h>
#include <linux/fsl_devices.h>
#include <linux/spi/spi.h>
#include <linux/spi/flash.h>
#include <linux/i2c.h>
#include <linux/i2c/pca953x.h>
#include <linux/ata.h>
#include <linux/mtd/mtd.h>
#include <linux/mtd/map.h>
#include <linux/mtd/partitions.h>
#include <linux/regulator/consumer.h>
#include <linux/pmic_external.h>
#include <linux/pmic_status.h>
#include <linux/ipu.h>
#include <linux/mxcfb.h>
#include <linux/pwm_backlight.h>
#include <linux/fec.h>
#include <linux/memblock.h>
#include <linux/gpio.h>
#include <linux/etherdevice.h>
#include <linux/regulator/anatop-regulator.h>
#include <linux/regulator/consumer.h>
#include <linux/regulator/machine.h>
#include <linux/regulator/fixed.h>
#include <linux/mfd/max17135.h>
#include <linux/mfd/wm8994/pdata.h>
#include <linux/mfd/wm8994/gpio.h>
#include <sound/wm8962.h>
#include <linux/mfd/mxc-hdmi-core.h>
#include <linux/i2c/at24.h>

#include <mach/common.h>
#include <mach/hardware.h>
#include <mach/mxc_dvfs.h>
#include <mach/memory.h>
#include <mach/iomux-mx6q.h>
#include <mach/imx-uart.h>
#include <mach/viv_gpu.h>
#include <mach/ahci_sata.h>
#include <mach/ipu-v3.h>
#include <mach/mxc_hdmi.h>
#include <mach/mxc_asrc.h>
#include <mach/mipi_dsi.h>

#include <asm/irq.h>
#include <asm/setup.h>
#include <asm/mach-types.h>
#include <asm/mach/arch.h>
#include <asm/mach/time.h>

#include "usb.h"
#include "devices-imx6q.h"
#include "crm_regs.h"
#include "cpu_op-mx6.h"
#include "board-mx6_kettos.h"

static int caam_enabled;

extern char *gp_reg_id;
extern char *soc_reg_id;
extern char *pu_reg_id;
extern int epdc_enabled;

static const struct esdhc_platform_data mx6q_kettos_sd2_data __initconst = {
	.cd_gpio = KETTOS_SD2_CD,
	.wp_gpio = KETTOS_SD2_WP,
	.keep_power_at_suspend = 1,
	.support_8bit = 1,
	.delay_line = 0,
	.cd_type = ESDHC_CD_CONTROLLER,
};

static const struct esdhc_platform_data mx6q_kettos_sd4_data __initconst = {
	.always_present = 1,
	.keep_power_at_suspend = 1,
	.support_8bit = 1,
	.delay_line = 0,
	.cd_type = ESDHC_CD_PERMANENT,
};

static const struct anatop_thermal_platform_data
	mx6q_kettos_anatop_thermal_data __initconst = {
		.name = "anatop_thermal",
};

static inline void mx6q_kettos_init_uart(void)
{
	if (cpu_is_mx6q())
		mxc_iomux_v3_setup_multiple_pads(mx6q_kettos_uart_pads, ARRAY_SIZE(mx6q_kettos_uart_pads));
	else
		mxc_iomux_v3_setup_multiple_pads(mx6dl_kettos_uart_pads, ARRAY_SIZE(mx6dl_kettos_uart_pads));

	imx6q_add_imx_uart(0, NULL);
}

static int mx6q_kettos_fec_phy_init(struct phy_device *phydev)
{
	unsigned short val;

	/*check phy power*/
	val = phy_read(phydev, 0x0);

	if (val & BMCR_PDOWN)
		phy_write(phydev, 0x0, (val & ~BMCR_PDOWN));

	return 0;
}

static struct fec_platform_data fec_data __initdata = {
	.init = mx6q_kettos_fec_phy_init,
	.phy = PHY_INTERFACE_MODE_RGMII,
#ifdef CONFIG_MX6_ENET_IRQ_TO_GPIO
	.gpio_irq = MX6_ENET_IRQ,
#endif
};

static struct mxc_audio_platform_data mx6_kettos_audio_data;

static int mx6_kettos_sgtl5000_init(void)
{
	struct clk *clko;
	struct clk *new_parent;
	int rate;

	clko = clk_get(NULL, "clko_clk");
	if (IS_ERR(clko)) {
		pr_err("can't get CLKO clock.\n");
		return PTR_ERR(clko);
	}

	new_parent = clk_get(NULL, "ahb");
	if (!IS_ERR(new_parent)) {
		clk_set_parent(clko, new_parent);
		clk_put(new_parent);
	}

	rate = clk_round_rate(clko, 16000000);
	if (rate < 8000000 || rate > 27000000) {
		pr_err("Error:SGTL5000 mclk freq %d out of range!\n", rate);
		clk_put(clko);
		return -1;
	}

	mx6_kettos_audio_data.sysclk = rate;
	clk_set_rate(clko, rate);
	clk_enable(clko);
	return 0;
}

static struct imx_ssi_platform_data mx6_kettos_ssi_pdata = {
	.flags = IMX_SSI_DMA | IMX_SSI_SYN,
};

static int mxc_sgtl5000_amp_enable(int enable)
{
	gpio_set_value(KETTOS_AMP_EN, enable ? 1 : 0);
	return 0;
}

static struct mxc_audio_platform_data mx6_kettos_audio_data = {
	.ssi_num = 1,
	.src_port = 2,
	.ext_port = 3,
	.init = mx6_kettos_sgtl5000_init,
	.hp_gpio = -1,
	.amp_enable = mxc_sgtl5000_amp_enable,
};

static struct platform_device mx6_kettos_audio_device = {
    .name = "imx-sgtl5000",
};

#ifdef CONFIG_SND_SOC_SGTL5000
static struct regulator_consumer_supply sgtl5000_kettos_consumer_vdda = {
	.supply = "VDDA",
	.dev_name = "1-000a",
};

static struct regulator_consumer_supply sgtl5000_kettos_consumer_vddio = {
	.supply = "VDDIO",
	.dev_name = "1-000a",
};

static struct regulator_consumer_supply sgtl5000_kettos_consumer_vddd = {
	.supply = "VDDD",
	.dev_name = "1-000a",
};

static struct regulator_init_data sgtl5000_kettos_vdda_reg_initdata = {
	.num_consumer_supplies = 1,
	.consumer_supplies = &sgtl5000_kettos_consumer_vdda,
};

static struct regulator_init_data sgtl5000_kettos_vddio_reg_initdata = {
	.num_consumer_supplies = 1,
	.consumer_supplies = &sgtl5000_kettos_consumer_vddio,
};

static struct regulator_init_data sgtl5000_kettos_vddd_reg_initdata = {
	.num_consumer_supplies = 1,
	.consumer_supplies = &sgtl5000_kettos_consumer_vddd,
};

static struct fixed_voltage_config sgtl5000_kettos_vdda_reg_config = {
	.supply_name	= "VDDA",
	.microvolts		= 2500000,
	.gpio			= -1,
	.init_data		= &sgtl5000_kettos_vdda_reg_initdata,
};

static struct fixed_voltage_config sgtl5000_kettos_vddio_reg_config = {
	.supply_name        = "VDDIO",
	.microvolts     = 3300000,
	.gpio           = -1,
	.init_data      = &sgtl5000_kettos_vddio_reg_initdata,
};

static struct fixed_voltage_config sgtl5000_kettos_vddd_reg_config = {
	.supply_name        = "VDDD",
	.microvolts     = 0,
	.gpio           = -1,
	.init_data      = &sgtl5000_kettos_vddd_reg_initdata,
};

static struct platform_device sgtl5000_kettos_vdda_reg_devices = {
	.name   = "reg-fixed-voltage",
	.id = 0,
	.dev    = {
		.platform_data = &sgtl5000_kettos_vdda_reg_config,
	},
};

static struct platform_device sgtl5000_kettos_vddio_reg_devices = {
	.name   = "reg-fixed-voltage",
	.id = 1,
	.dev    = {
		.platform_data = &sgtl5000_kettos_vddio_reg_config,
	},
};

static struct platform_device sgtl5000_kettos_vddd_reg_devices = {
	.name   = "reg-fixed-voltage",
	.id = 2,
	.dev    = {
		.platform_data = &sgtl5000_kettos_vddd_reg_config,
	},
};
#endif /* CONFIG_SND_SOC_SGTL5000 */

static struct imx_asrc_platform_data imx_asrc_data = {
	.channel_bits = 4,
	.clk_map_ver = 2,
};

static int imx6q_init_audio(void)
{
	gpio_request(KETTOS_AMP_EN, "audioamp-stdby");
	gpio_direction_output(KETTOS_AMP_EN, 0);
	gpio_request(KETTOS_CODEC_EN, "codec-en");
	gpio_direction_output(KETTOS_CODEC_EN, 1);

	imx_asrc_data.asrc_core_clk = clk_get(NULL, "asrc_clk");
	imx_asrc_data.asrc_audio_clk = clk_get(NULL, "asrc_serial_clk");
	imx6q_add_asrc(&imx_asrc_data);

	mx6_kettos_audio_device.dev.platform_data = &mx6_kettos_audio_data;
	platform_device_register(&mx6_kettos_audio_device);
	imx6q_add_imx_ssi(1, &mx6_kettos_ssi_pdata);

	#ifdef CONFIG_SND_SOC_SGTL5000
	platform_device_register(&sgtl5000_kettos_vdda_reg_devices);
	platform_device_register(&sgtl5000_kettos_vddio_reg_devices);
	platform_device_register(&sgtl5000_kettos_vddd_reg_devices);
	#endif
	return 0;
}

static void kettos_eeprom_setup(struct memory_accessor *mem_acc, void *context)
{
	char eeprom_read_test_string[20];
	int ret = 0;
	#define EEPROM_READ_STR_OFFSET  0x0
	ret = mem_acc->read(mem_acc, (char *)&eeprom_read_test_string,
		EEPROM_READ_STR_OFFSET, sizeof(eeprom_read_test_string));

	printk("%s:Test on Baseboard Identification.\n",__FUNCTION__);
	/*This should access content of eeprom*/
}

static struct at24_platform_data kettos_at24c08_pdata = {
	.byte_len       = SZ_8K / 8,
	.page_size      = 16,
	.flags          = AT24_FLAG_ADDR16,
	.setup          = kettos_eeprom_setup,
	.context        = (void *)NULL,
};

static struct imxi2c_platform_data mx6q_kettos_i2c_data = {
	.bitrate = 100000,
};

static struct i2c_board_info mxc_i2c0_board_info[] __initdata = {
	{
		I2C_BOARD_INFO("prism", 0x10),
		.irq    = -EINVAL,
		.flags = I2C_CLIENT_WAKE,
	},
};

static struct i2c_board_info mxc_i2c1_board_info[] __initdata = {
	{
		I2C_BOARD_INFO("sgtl5000", 0x0a),
	},
};

static struct i2c_board_info mxc_i2c2_board_info[] __initdata = {
	{
		I2C_BOARD_INFO("24c08", 0x50),
		.platform_data  = &kettos_at24c08_pdata,
	}
};

static void imx6q_kettos_usbotg_vbus(bool on)
{
	if (on)
		gpio_set_value(KETTOS_OTG_EN, 1);
	else
		gpio_set_value(KETTOS_OTG_EN, 0);
}

static void __init imx6q_kettos_init_usb(void)
{
	imx_otg_base = MX6_IO_ADDRESS(MX6Q_USB_OTG_BASE_ADDR);
	/* disable external charger detect,
	 * or it will affect signal quality at dp .
	 */

	#if 0
	ret = gpio_request(POS_USB_OTG_OC, "usb-oc");
	if (ret) {
		pr_err("failed to get GPIO SABRESD_USB_OTG_OC: %d\n", ret);
		return;
	}
	gpio_direction_input(POS_USB_OTG_OC);

	ret = gpio_request(POS_USB_HOST1_OC, "usb1-oc");
	if (ret) {
		pr_err("failed to get GPIO SABRESD_USB_HOST1_OC: %d\n", ret);
		return;
	}
	gpio_direction_input(POS_USB_HOST1_OC);
	#endif

	if (board_is_mx6_reva())
		mxc_iomux_set_gpr_register(1, 13, 1, 1);
	else
		mxc_iomux_set_gpr_register(1, 13, 1, 0);

	mx6_set_otghost_vbus_func(imx6q_kettos_usbotg_vbus);
}

static struct viv_gpu_platform_data imx6q_gpu_pdata __initdata = {
	.reserved_mem_size = SZ_128M,
};

static struct ipuv3_fb_platform_data kettos_fb_data[] = {
	{
	.disp_dev = "lcd",
	.interface_pix_fmt = IPU_PIX_FMT_RGB24,
	.mode_str = "LCD_WVGA",
	.default_bpp = 32,
	.int_clk = true,
	.late_init = false,
	},
};

static struct fsl_mxc_lcd_platform_data lcdif_data = {
	.ipu_id = 0,
	.disp_id = 0,
	.default_ifmt = IPU_PIX_FMT_RGB565,
};

static struct imx_ipuv3_platform_data ipu_data[] = {
	{
	.rev = 4,
	.csi_clk[0] = "clko_clk",
	.bypass_reset = false,
	}, {
	.rev = 4,
	.csi_clk[0] = "clko_clk",
	.bypass_reset = false,
	},
};

static void kettos_suspend_enter(void)
{
}

static void kettos_suspend_exit(void)
{
}
static const struct pm_platform_data mx6q_kettos_pm_data __initconst = {
	.name = "imx_pm",
	.suspend_enter = kettos_suspend_enter,
	.suspend_exit = kettos_suspend_exit,
};

static struct regulator_consumer_supply kettos_vmmc_consumers[] = {
	REGULATOR_SUPPLY("vmmc", "sdhci-esdhc-imx.1"),
	REGULATOR_SUPPLY("vmmc", "sdhci-esdhc-imx.2"),
	REGULATOR_SUPPLY("vmmc", "sdhci-esdhc-imx.3"),
};

static struct regulator_init_data kettos_vmmc_init = {
	.num_consumer_supplies = ARRAY_SIZE(kettos_vmmc_consumers),
	.consumer_supplies = kettos_vmmc_consumers,
};

static struct fixed_voltage_config kettos_vmmc_reg_config = {
	.supply_name		= "vmmc",
	.microvolts		= 3300000,
	.gpio			= -1,
	.init_data		= &kettos_vmmc_init,
};

static struct platform_device kettos_vmmc_reg_devices = {
	.name	= "reg-fixed-voltage",
	.id	= 3,
	.dev	= {
		.platform_data = &kettos_vmmc_reg_config,
	},
};

/* ----- Power Key -------- */
#if defined(CONFIG_KEYBOARD_GPIO) || defined(CONFIG_KEYBOARD_GPIO_MODULE)
#define GPIO_BUTTON(gpio_num, ev_code, act_low, descr, wake, debounce)  \
{                               \
    .gpio       = gpio_num,             \
    .type       = EV_KEY,               \
    .code       = ev_code,              \
    .active_low = act_low,              \
    .desc       = "btn " descr,             \
    .wakeup     = wake,                 \
    .debounce_interval = debounce,              \
}

static struct gpio_keys_button new_kettos_buttons[] = {
	GPIO_BUTTON(KETTOS_SW_HOME, KEY_HOMEPAGE, 1, "home-key", 0, 1),
	GPIO_BUTTON(KETTOS_SW_BACK, KEY_BACK, 1, "back-key", 0, 1),
	GPIO_BUTTON(KETTOS_SW_MENU, KEY_MENU, 1, "menu-key", 0, 1),
};

static struct gpio_keys_platform_data new_kettos_button_data = {
	.buttons    = new_kettos_buttons,
	.nbuttons   = ARRAY_SIZE(new_kettos_buttons),
};

static struct platform_device kettos_button_device = {
	.name	= "gpio-keys",
	.id		= -1,
	.num_resources  = 0,
};

static void __init imx6q_add_device_buttons(void)
{
	platform_device_add_data(&kettos_button_device,
		&new_kettos_button_data, sizeof(new_kettos_button_data));
	platform_device_register(&kettos_button_device);
}
#else
static void __init imx6q_add_device_buttons(void) {}
#endif
/* ------------------------ */

static struct platform_pwm_backlight_data mx6_kettos_pwm_backlight_data = {
	.pwm_id = 0,
	.max_brightness = 248,
	.dft_brightness = 128,
	.pwm_period_ns = 50000,
};

static struct mxc_dvfs_platform_data kettos_dvfscore_data = {
	.reg_id = "VDDCORE",
	.soc_id	= "VDDSOC",
	.clk1_id = "cpu_clk",
	.clk2_id = "gpc_dvfs_clk",
	.gpc_cntr_offset = MXC_GPC_CNTR_OFFSET,
	.ccm_cdcr_offset = MXC_CCM_CDCR_OFFSET,
	.ccm_cacrr_offset = MXC_CCM_CACRR_OFFSET,
	.ccm_cdhipr_offset = MXC_CCM_CDHIPR_OFFSET,
	.prediv_mask = 0x1F800,
	.prediv_offset = 11,
	.prediv_val = 3,
	.div3ck_mask = 0xE0000000,
	.div3ck_offset = 29,
	.div3ck_val = 2,
	.emac_val = 0x08,
	.upthr_val = 25,
	.dnthr_val = 9,
	.pncthr_val = 33,
	.upcnt_val = 10,
	.dncnt_val = 10,
	.delay_time = 80,
};

static void __init fixup_mxc_board(struct machine_desc *desc, struct tag *tags,
				   char **cmdline, struct meminfo *mi)
{
}

static int __init caam_setup(char *__unused)
{
	caam_enabled = 1;
	return 1;
}
early_param("caam", caam_setup);

#define SNVS_LPCR 0x38
static void mx6_snvs_poweroff(void)
{

	void __iomem *mx6_snvs_base =  MX6_IO_ADDRESS(MX6Q_SNVS_BASE_ADDR);
	u32 value;
	value = readl(mx6_snvs_base + SNVS_LPCR);
	/*set TOP and DP_EN bit*/
	writel(value | 0x60, mx6_snvs_base + SNVS_LPCR);
}

static void init_prism_touch()
{
	gpio_request(KETTOS_TH_ATT, "kettos-th-att");
	gpio_direction_input(KETTOS_TH_ATT);
	gpio_request(KETTOS_TH_REST, "kettos-th-rest");
	gpio_direction_output(KETTOS_TH_REST, 1);
	gpio_set_value(KETTOS_TH_REST, 0);
	mdelay(50);
	gpio_set_value(KETTOS_TH_REST, 1);

	mxc_i2c0_board_info[0].irq = gpio_to_irq(KETTOS_TH_ATT);
}

static void mx6_kettos_init_panel()
{
	int i = 0;
	/*
	 * MX6DL/Solo only supports single IPU
	 * The2 following codes are used to change ipu id
	 * and display id information for MX6DL/Solo. Then
	 * register 1 IPU device and up to 2 displays for
	 * MX6DL/Solo
	 */

	imx6q_add_ipuv3(0, &ipu_data[0]);
	if (cpu_is_mx6q()) {
		imx6q_add_ipuv3(1, &ipu_data[1]);
		for (i = 0; i < 4 && i < ARRAY_SIZE(kettos_fb_data); i++)
		imx6q_add_ipuv3fb(i, &kettos_fb_data[i]);
	} else
		for (i = 0; i < 2 && i < ARRAY_SIZE(kettos_fb_data); i++)
			imx6q_add_ipuv3fb(i, &kettos_fb_data[i]);
	gpio_request(KETTOS_BKL_EN, "kettos-bkl-en");
	gpio_direction_output(KETTOS_BKL_EN, 1);
	gpio_set_value(KETTOS_BKL_EN, 1);
	imx6q_add_lcdif(&lcdif_data);
}

/*!
 * Board specific initialization.
 */
static void __init mx6_kettos_board_init(void)
{
	int i;
	struct clk *clko, *clko2;
	struct clk *new_parent;
	int rate;

	if (cpu_is_mx6q())
		mxc_iomux_v3_setup_multiple_pads(mx6q_kettos_pads,
			ARRAY_SIZE(mx6q_kettos_pads));
	else if (cpu_is_mx6dl()) {
		mxc_iomux_v3_setup_multiple_pads(mx6dl_kettos_pads,
			ARRAY_SIZE(mx6dl_kettos_pads));
	}

	#ifdef CONFIG_FEC_1588
	/* Set GPIO_16 input for IEEE-1588 ts_clk and RMII reference clock
	 * For MX6 GPR1 bit21 meaning:
	 * Bit21:       0 - GPIO_16 pad output
	 *              1 - GPIO_16 pad input
	 */
	 mxc_iomux_set_gpr_register(1, 21, 1, 1);
	#endif

	gp_reg_id = kettos_dvfscore_data.reg_id;
	soc_reg_id = kettos_dvfscore_data.soc_id;
	mx6q_kettos_init_uart();
	mx6_kettos_init_panel();
	imx6q_add_vdoa();
	imx6q_add_v4l2_output(0);

	if (1 == caam_enabled)
		imx6q_add_imx_caam();

	init_prism_touch();

	imx6q_add_imx_i2c(0, &mx6q_kettos_i2c_data);
	imx6q_add_imx_i2c(1, &mx6q_kettos_i2c_data);
	imx6q_add_imx_i2c(2, &mx6q_kettos_i2c_data);

	i2c_register_board_info(0, mxc_i2c0_board_info, ARRAY_SIZE(mxc_i2c0_board_info));
	i2c_register_board_info(1, mxc_i2c1_board_info, ARRAY_SIZE(mxc_i2c1_board_info));
	i2c_register_board_info(2, mxc_i2c2_board_info, ARRAY_SIZE(mxc_i2c2_board_info));

	imx6q_add_anatop_thermal_imx(1, &mx6q_kettos_anatop_thermal_data);
	imx6_init_fec(fec_data);
#ifdef CONFIG_MX6_ENET_IRQ_TO_GPIO
	/* Make sure the IOMUX_OBSRV_MUX1 is set to ENET_IRQ. */
	mxc_iomux_set_specialbits_register(IOMUX_OBSRV_MUX1_OFFSET,
		OBSRV_MUX1_ENET_IRQ, OBSRV_MUX1_MASK);
#endif

	imx6q_add_pm_imx(0, &mx6q_kettos_pm_data);

	/* Move sd4 to first because sd4 connect to emmc.
	   Mfgtools want emmc is mmcblk0 and other sd card is mmcblk1.
	*/
	imx6q_add_sdhci_usdhc_imx(3, &mx6q_kettos_sd4_data);
	imx6q_add_sdhci_usdhc_imx(1, &mx6q_kettos_sd2_data);
	imx_add_viv_gpu(&imx6_gpu_data, &imx6q_gpu_pdata);
	imx6q_kettos_init_usb();
	imx6q_add_vpu();
	imx6q_init_audio();
	platform_device_register(&kettos_vmmc_reg_devices);
	imx6q_add_device_buttons();

	/*
	 * Disable HannStar touch panel CABC function,
	 * this function turns the panel's backlight automatically
	 * according to the content shown on the panel which
	 * may cause annoying unstable backlight issue.
	 */

	imx6q_add_mxc_pwm(0);
	imx6q_add_mxc_pwm(1);
	imx6q_add_mxc_pwm(2);
	imx6q_add_mxc_pwm(3);
	imx6q_add_mxc_pwm_backlight(0, &mx6_kettos_pwm_backlight_data);

	imx6q_add_otp();
	imx6q_add_viim();
	imx6q_add_imx2_wdt(0, NULL);
	imx6q_add_dma();

	imx6q_add_dvfs_core(&kettos_dvfscore_data);

	if (cpu_is_mx6dl()) {
		imx6dl_add_imx_pxp();
		imx6dl_add_imx_pxp_client();
	}

	clko2 = clk_get(NULL, "clko2_clk");
	if (IS_ERR(clko2))
		pr_err("can't get CLKO2 clock.\n");

	new_parent = clk_get(NULL, "osc_clk");
	if (!IS_ERR(new_parent)) {
		clk_set_parent(clko2, new_parent);
		clk_put(new_parent);
	}
	rate = clk_round_rate(clko2, 24000000);
	clk_set_rate(clko2, rate);
	clk_enable(clko2);

	/* Camera and audio use osc clock */
	clko = clk_get(NULL, "clko_clk");
	if (!IS_ERR(clko))
		clk_set_parent(clko, clko2);

	/* Register charger chips */
	pm_power_off = mx6_snvs_poweroff;
	imx6q_add_busfreq();

	imx6_add_armpmu();
	imx6q_add_perfmon(0);
	imx6q_add_perfmon(1);
	imx6q_add_perfmon(2);
}

extern void __iomem *twd_base;
static void __init mx6_kettos_timer_init(void)
{
	struct clk *uart_clk;
#ifdef CONFIG_LOCAL_TIMERS
	twd_base = ioremap(LOCAL_TWD_ADDR, SZ_256);
	BUG_ON(!twd_base);
#endif
	mx6_clocks_init(32768, 24000000, 0, 0);

	uart_clk = clk_get_sys("imx-uart.0", NULL);
	early_console_setup(UART1_BASE_ADDR, uart_clk);
}

static struct sys_timer mx6_kettos_timer = {
	.init   = mx6_kettos_timer_init,
};

static void __init mx6q_kettos_reserve(void)
{
#if defined(CONFIG_MXC_GPU_VIV) || defined(CONFIG_MXC_GPU_VIV_MODULE)
	phys_addr_t phys;

	if (imx6q_gpu_pdata.reserved_mem_size) {
		phys = memblock_alloc_base(imx6q_gpu_pdata.reserved_mem_size,
					   SZ_4K, SZ_1G);
		memblock_remove(phys, imx6q_gpu_pdata.reserved_mem_size);
		imx6q_gpu_pdata.reserved_mem_base = phys;
	}
#endif
}

/*
 * initialize __mach_desc_MX6Q_SABRESD data structure.
 */
MACHINE_START(MX6DL_KETTOS, "Freescale i.MX 6Quad/DualLite/Solo POS Board")
	/* Maintainer: Freescale Semiconductor, Inc. */
	.boot_params = MX6_PHYS_OFFSET + 0x100,
	.fixup = fixup_mxc_board,
	.map_io = mx6_map_io,
	.init_irq = mx6_init_irq,
	.init_machine = mx6_kettos_board_init,
	.timer = &mx6_kettos_timer,
	.reserve = mx6q_kettos_reserve,
MACHINE_END
