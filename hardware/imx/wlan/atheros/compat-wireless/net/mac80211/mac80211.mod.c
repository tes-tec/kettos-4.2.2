#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);

struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
 .name = KBUILD_MODNAME,
 .init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
 .exit = cleanup_module,
#endif
 .arch = MODULE_ARCH_INIT,
};

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0xf997ecc4, "module_layout" },
	{ 0x63519996, "register_netdevice" },
	{ 0x609f1c7e, "synchronize_net" },
	{ 0x92b57248, "flush_work" },
	{ 0x767a04a1, "kmem_cache_destroy" },
	{ 0xe90dcae0, "__request_module" },
	{ 0x12da5bb2, "__kmalloc" },
	{ 0x6ce5bf62, "cfg80211_cqm_rssi_notify" },
	{ 0xe8611953, "perf_tp_event" },
	{ 0xf9a482f9, "msleep" },
	{ 0xd5b037e1, "kref_put" },
	{ 0xab6babaf, "pm_qos_request" },
	{ 0xd907e7e8, "init_dummy_netdev" },
	{ 0xff178f6, "__aeabi_idivmod" },
	{ 0x948a93e4, "wiphy_free" },
	{ 0x6f18b723, "cfg80211_unlink_bss" },
	{ 0x311b7963, "_raw_spin_unlock" },
	{ 0x3ec8886f, "param_ops_int" },
	{ 0x91eb9b4, "round_jiffies" },
	{ 0x2e5810c6, "__aeabi_unwind_cpp_pr1" },
	{ 0xacd8bb2d, "del_timer" },
	{ 0x1879fcbd, "bridge_tunnel_header" },
	{ 0x97255bdf, "strlen" },
	{ 0xeb2f057b, "led_trigger_event" },
	{ 0x60a13e90, "rcu_barrier" },
	{ 0xdef8b1c3, "__alloc_workqueue_key" },
	{ 0x9c64fbd, "ieee80211_frequency_to_channel" },
	{ 0x3e45e9ff, "register_inetaddr_notifier" },
	{ 0x79aa04a2, "get_random_bytes" },
	{ 0x7bd9314f, "dev_printk" },
	{ 0xa3448344, "napi_complete" },
	{ 0x9c0ba064, "malloc_sizes" },
	{ 0x2ab623c1, "cfg80211_inform_bss_frame" },
	{ 0xc7a4fbed, "rtnl_lock" },
	{ 0xca01666d, "_raw_read_lock" },
	{ 0xb54533f7, "usecs_to_jiffies" },
	{ 0x1feee434, "netif_carrier_on" },
	{ 0x8bd94317, "_raw_spin_lock_bh" },
	{ 0x6d53ae3c, "skb_clone" },
	{ 0x7ef39823, "ieee80211_hdrlen" },
	{ 0xf7802486, "__aeabi_uidivmod" },
	{ 0x1ba27dcd, "skb_copy" },
	{ 0x2dc35ea, "led_trigger_register" },
	{ 0x8949858b, "schedule_work" },
	{ 0x2a3aa678, "_test_and_clear_bit" },
	{ 0x63ecad53, "register_netdevice_notifier" },
	{ 0xda4cedff, "netif_carrier_off" },
	{ 0x4205ad24, "cancel_work_sync" },
	{ 0x6860415, "led_blink_set" },
	{ 0x69dd380f, "queue_work" },
	{ 0xe2fae716, "kmemdup" },
	{ 0x452b9ff3, "cfg80211_rx_mgmt" },
	{ 0x7513e94e, "ieee80211_channel_to_frequency" },
	{ 0x86cb7b28, "init_timer_key" },
	{ 0x9621849f, "ring_buffer_event_data" },
	{ 0xecbf78f3, "mutex_unlock" },
	{ 0xc63f1b81, "ieee80211_radiotap_iterator_next" },
	{ 0xbff0ec54, "cfg80211_send_rx_assoc" },
	{ 0x62b6b404, "ieee80211_data_to_8023" },
	{ 0x240a71d8, "cfg80211_probe_status" },
	{ 0x34a29bbd, "cfg80211_del_sta" },
	{ 0x93ea6018, "filter_current_check_discard" },
	{ 0x1251d30f, "call_rcu" },
	{ 0xb441c94, "idr_for_each" },
	{ 0x39e15e5f, "trace_nowake_buffer_unlock_commit" },
	{ 0x7d11c268, "jiffies" },
	{ 0xfe769456, "unregister_netdevice_notifier" },
	{ 0x82ba902f, "skb_trim" },
	{ 0xe2d5255a, "strcmp" },
	{ 0xf7b2f52a, "cfg80211_mgmt_tx_status" },
	{ 0x9bbb7d37, "netif_rx" },
	{ 0xbc609ef3, "led_brightness_set" },
	{ 0x1f178ad7, "__pskb_pull_tail" },
	{ 0x865029ac, "__hw_addr_sync" },
	{ 0xe707d823, "__aeabi_uidiv" },
	{ 0x72aa82c6, "param_ops_charp" },
	{ 0x69b18f43, "rfc1042_header" },
	{ 0xfa2a45e, "__memzero" },
	{ 0xf7b574c2, "del_timer_sync" },
	{ 0xeaf9bfa1, "skb_queue_purge" },
	{ 0xc1c3deaa, "trace_define_field" },
	{ 0x5f754e5a, "memset" },
	{ 0x557fcb0f, "netif_rx_ni" },
	{ 0x9fdecc31, "unregister_netdevice_many" },
	{ 0x3eee6008, "__ieee80211_get_channel" },
	{ 0xebf82e88, "dev_alloc_skb" },
	{ 0x8f0ed7ad, "idr_destroy" },
	{ 0xb86e4ab9, "random32" },
	{ 0x4cdb5b0b, "dev_err" },
	{ 0x74c97f9c, "_raw_spin_unlock_irqrestore" },
	{ 0x9e0851ce, "cfg80211_get_bss" },
	{ 0x37befc70, "jiffies_to_msecs" },
	{ 0x8195831a, "__mutex_init" },
	{ 0x27e1a049, "printk" },
	{ 0x254130e0, "__cfg80211_auth_canceled" },
	{ 0xaf91d89f, "__kernel_param_lock" },
	{ 0x4141f80, "__tracepoint_module_get" },
	{ 0x71c90087, "memcmp" },
	{ 0xf712eb17, "cfg80211_notify_new_peer_candidate" },
	{ 0x982e6b6d, "ieee80211_radiotap_iterator_init" },
	{ 0x60313c1c, "led_trigger_unregister" },
	{ 0x7d02520f, "_raw_read_unlock" },
	{ 0xbe5cabd4, "cfg80211_send_disassoc" },
	{ 0x96cbcf31, "pm_qos_add_notifier" },
	{ 0x5434d539, "free_netdev" },
	{ 0x8ce417b7, "wiphy_unregister" },
	{ 0xfaef0ed, "__tasklet_schedule" },
	{ 0x1d216618, "netif_receive_skb" },
	{ 0x73e20c1c, "strlcpy" },
	{ 0x9742536a, "kmem_cache_free" },
	{ 0x16305289, "warn_slowpath_null" },
	{ 0x304ac4d6, "skb_push" },
	{ 0xe15044f, "mutex_lock" },
	{ 0xa34f1ef5, "crc32_le" },
	{ 0x98c3dab2, "destroy_workqueue" },
	{ 0xddcb99d9, "cfg80211_send_rx_auth" },
	{ 0x97ddd3de, "dev_close" },
	{ 0x37fa1729, "cfg80211_cqm_pktloss_notify" },
	{ 0xa530aee3, "cfg80211_michael_mic_failure" },
	{ 0x9545af6d, "tasklet_init" },
	{ 0x6ed4557e, "mod_timer" },
	{ 0x56bb28ec, "netif_napi_add" },
	{ 0x2469810f, "__rcu_read_unlock" },
	{ 0x22d88e1d, "add_timer" },
	{ 0x9f7ad740, "cfg80211_report_obss_beacon" },
	{ 0x6091797f, "synchronize_rcu" },
	{ 0x43b0c9c3, "preempt_schedule" },
	{ 0x7ca212c5, "skb_pull" },
	{ 0xfef8a166, "trace_current_buffer_lock_reserve" },
	{ 0x1931d52, "__cfg80211_send_disassoc" },
	{ 0xc20e3d55, "cfg80211_ibss_joined" },
	{ 0xa921262c, "cfg80211_rx_spurious_frame" },
	{ 0x8c2c6855, "flush_workqueue" },
	{ 0x2196324, "__aeabi_idiv" },
	{ 0x3bc961db, "dev_kfree_skb_any" },
	{ 0x9f46ced8, "__sw_hweight64" },
	{ 0x3df1167f, "idr_remove" },
	{ 0x82072614, "tasklet_kill" },
	{ 0x19ecc7ec, "ieee80211_bss_get_ie" },
	{ 0x3cdfe192, "dev_kfree_skb_irq" },
	{ 0x66f4145e, "idr_pre_get" },
	{ 0xf4c3ae77, "ftrace_event_reg" },
	{ 0x3dc92d65, "module_put" },
	{ 0xc89aa3cb, "skb_queue_tail" },
	{ 0x3ff62317, "local_bh_disable" },
	{ 0x89143fcd, "skb_copy_expand" },
	{ 0xa12d51ac, "cfg80211_gtk_rekey_notify" },
	{ 0xa71d8700, "_dev_info" },
	{ 0x66439ba5, "kmem_cache_alloc" },
	{ 0xd7779230, "cfg80211_put_bss" },
	{ 0x77c57ffd, "__alloc_skb" },
	{ 0x7cc35c7c, "wiphy_new" },
	{ 0x760b437a, "unregister_inetaddr_notifier" },
	{ 0x6d6c3412, "wiphy_register" },
	{ 0x3b086c98, "__napi_schedule" },
	{ 0x10411a86, "ieee802_11_parse_elems_crc" },
	{ 0xb368ec89, "_raw_spin_unlock_bh" },
	{ 0x2fe0ccd9, "cfg80211_send_unprot_disassoc" },
	{ 0x96612380, "cfg80211_classify8021d" },
	{ 0x3bd1b1f6, "msecs_to_jiffies" },
	{ 0xb008c36c, "kfree_skb" },
	{ 0x689aa88f, "cfg80211_ready_on_channel" },
	{ 0x6b2dc060, "dump_stack" },
	{ 0x799aca4, "local_bh_enable" },
	{ 0x7609e84b, "alloc_netdev_mqs" },
	{ 0x5d9aaeab, "eth_type_trans" },
	{ 0x6a561abc, "crypto_destroy_tfm" },
	{ 0xc27487dd, "__bug" },
	{ 0x50fad434, "round_jiffies_up" },
	{ 0xd11c0dc1, "__kernel_param_unlock" },
	{ 0x142df792, "pskb_expand_head" },
	{ 0xab6b94f7, "ether_setup" },
	{ 0x94dea09a, "kmem_cache_alloc_trace" },
	{ 0xc2161e33, "_raw_spin_lock" },
	{ 0x807c11dc, "cfg80211_send_unprot_deauth" },
	{ 0x341dbfa3, "__per_cpu_offset" },
	{ 0xbd7083bc, "_raw_spin_lock_irqsave" },
	{ 0x7b412fb0, "kmem_cache_create" },
	{ 0x1763264b, "unregister_netdevice_queue" },
	{ 0x58e07f49, "cfg80211_new_sta" },
	{ 0xefdd5a63, "ktime_get_ts" },
	{ 0xf6ebc03b, "net_ratelimit" },
	{ 0xb8ea6565, "_raw_write_unlock_bh" },
	{ 0x8bded8a7, "__cfg80211_send_deauth" },
	{ 0x8d66a3a, "warn_slowpath_fmt" },
	{ 0xee3b5c62, "_raw_read_lock_bh" },
	{ 0x874be1d6, "_raw_read_unlock_bh" },
	{ 0x83800bfa, "kref_init" },
	{ 0x6d27ef64, "__bitmap_empty" },
	{ 0x1eb9516e, "round_jiffies_relative" },
	{ 0x37a0cba, "kfree" },
	{ 0x9d669763, "memcpy" },
	{ 0xeba4003a, "trace_event_raw_init" },
	{ 0x8b89960d, "___pskb_trim" },
	{ 0xd4bb89b5, "dev_alloc_name" },
	{ 0xb7b61546, "crc32_be" },
	{ 0x6128b5fc, "__printk_ratelimit" },
	{ 0x9775cdc, "kref_get" },
	{ 0xd788742d, "perf_trace_buf_prepare" },
	{ 0x3e9110fa, "__hw_addr_unsync" },
	{ 0x86cd8574, "cfg80211_send_deauth" },
	{ 0x5c212fe5, "ieee80211_amsdu_to_8023s" },
	{ 0x6d09b57e, "_raw_write_lock_bh" },
	{ 0x310158ba, "skb_dequeue" },
	{ 0x9ab5538a, "dev_warn" },
	{ 0xf6e04730, "event_storage" },
	{ 0x722c7f97, "cfg80211_remain_on_channel_expired" },
	{ 0xae0c87ee, "pm_qos_remove_notifier" },
	{ 0xefd6cf06, "__aeabi_unwind_cpp_pr0" },
	{ 0x53143c2d, "cfg80211_send_auth_timeout" },
	{ 0x676bbc0f, "_set_bit" },
	{ 0x701d0ebd, "snprintf" },
	{ 0xf439f380, "__netif_schedule" },
	{ 0xca54fee, "_test_and_set_bit" },
	{ 0xaf51a717, "trace_seq_printf" },
	{ 0x99bb8806, "memmove" },
	{ 0x9f613474, "consume_skb" },
	{ 0x139c3088, "cfg80211_sched_scan_stopped" },
	{ 0x85670f1d, "rtnl_is_locked" },
	{ 0x57674fd7, "__sw_hweight16" },
	{ 0x4022a63b, "cfg80211_scan_done" },
	{ 0xed8aaba2, "idr_init" },
	{ 0x13e1f1fa, "dev_queue_xmit" },
	{ 0x8d522714, "__rcu_read_lock" },
	{ 0x49ebacbd, "_clear_bit" },
	{ 0x6a20607e, "skb_put" },
	{ 0x516b322b, "eth_mac_addr" },
	{ 0xa8e0681b, "crypto_alloc_base" },
	{ 0xa170bbdb, "outer_cache" },
	{ 0xf60fdde3, "idr_find" },
	{ 0xee36ec33, "skb_copy_bits" },
	{ 0x6bee8846, "event_storage_mutex" },
	{ 0x39f29fe9, "queue_delayed_work" },
	{ 0x6e720ff2, "rtnl_unlock" },
	{ 0x6cd0dca8, "cfg80211_rx_unexpected_4addr_frame" },
	{ 0xf389fe60, "__hw_addr_init" },
	{ 0xe914e41e, "strcpy" },
	{ 0x24055f73, "cfg80211_sched_scan_results" },
	{ 0x4a2b20b7, "cfg80211_send_assoc_timeout" },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=cfg80211";

