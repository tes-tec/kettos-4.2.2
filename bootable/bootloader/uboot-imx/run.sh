#!/bin/bash

LOCAL_PATH=`pwd`
ARCH=arm
CROSS_COMPILE=$LOCAL_PATH/../../../prebuilts/gcc/linux-x86/arm/arm-eabi-4.6/bin/arm-eabi-
JOBS=$1

if [ $1 == "distclean" ]; then
	make ARCH=$ARCH CROSS_COMPILE=$CROSS_COMPILE distclean
	exit;
fi

if [ $1 == "linux" ]; then
	make ARCH=$ARCH CROSS_COMPILE=$CROSS_COMPILE mx6dl_sabresd_config
	JOBS=$2
fi

if [ $1 == "kettos" ]; then
	make ARCH=$ARCH CROSS_COMPILE=$CROSS_COMPILE mx6_kettos_config
	JOBS=$2
fi

if [ $1 == "android" ]; then
	make ARCH=$ARCH CROSS_COMPILE=$CROSS_COMPILE mx6_kettos_android_config
	JOBS=$2
fi

make ARCH=$ARCH CROSS_COMPILE=$CROSS_COMPILE $JOBS

