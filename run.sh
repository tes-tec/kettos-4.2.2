#!/bin/bash

if [ $1 == "clean" ]; then
	echo "<< Remove out/ >>"
	rm -rf out
	echo "<< Remove wi-fi tmp files >>"
	rm -r hardware/imx/wlan/atheros
	git checkout hardware/imx/wlan/atheros
	echo "<< clean kernel >>"
	cd kernel_imx
	./run.sh distclean
	echo "<< clean u-boot >>"
	cd ../bootable/bootloader/uboot-imx/
	./run.sh distclean
	cd ../../../
	exit;
fi

MODE=eng

source build/envsetup.sh
lunch kettos-${MODE}
time make $1 $2 $3
